from django.urls import path
from rest_framework import routers
from client.apiviews import ProductViewSet
from client.views import *

router = routers.DefaultRouter()
router.register('products', ProductViewSet)

urlpatterns = [
    path("login/", LoginView.as_view(), name='login'),
    path("signin/", SigninView.as_view(), name='signin'),
    path("logout/", LogoutView.as_view(), name='logout'),
    path("", home_view, name="home")
]
