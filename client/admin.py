from django.contrib import admin
from client.models import *


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', 'username', 'role']

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'type', 'amount']

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['date', 'client']

@admin.register(DetailOrder)
class DetailOrderAdmin(admin.ModelAdmin):
    list_display = ['order', 'product', 'quantity']