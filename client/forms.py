from random import choices
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django import forms

from client.models import Client


class LoginForm(AuthenticationForm):
    pass

class SigninForm(UserCreationForm):
    # username and password are charged for use UserCreationForm
    CHOICES_CLIENT = (('domiciliary', 'Domiciliary'), ('client', 'Client'))
    email = forms.EmailField(label='Email')
    first_name = forms.CharField(label='First Name')
    last_name = forms.CharField(label='Last Name')
    role = forms.ChoiceField(label='Role',choices=CHOICES_CLIENT)

    class Meta:
        model = Client
        fields = ('username', 'first_name', 'last_name', 'email', 'address', 'role')
