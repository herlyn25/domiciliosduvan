from rest_framework.viewsets import ModelViewSet
from client.models import Product
from client.serializer import ProductSerializer


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer