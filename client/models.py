from django.contrib.auth.models import AbstractUser
from django.db import models


class Client(AbstractUser):
    CHOICES_CLIENT = (('domiciliary', 'Domiciliary'), ('client', 'Client'), ('admin', 'Admin'))
    address = models.CharField(max_length=100)
    role = models.CharField(max_length=25, choices=CHOICES_CLIENT)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Product(models.Model):
    CHOICES_TYPE = (('drink', 'Drink'), ('saucer', 'Saucer'))
    name = models.CharField(max_length=150)
    type = models.CharField(max_length=20, choices=CHOICES_TYPE)
    amount = models.FloatField()

    def __str__(self):
        return self.name


class Order(models.Model):
    CHOICES_TYPE = (('preparacion', 'Preparacion'),
                    ('servida', 'Servida'),
                    ('en camino', 'En camino'),
                    ('entregada', "Entregada"))
    date = models.DateField(auto_now_add=True)
    client = models.ForeignKey(Client, limit_choices_to={'role': 'client'}, on_delete=models.CASCADE)
    state = models.CharField(max_length=20, choices=CHOICES_TYPE, default="preparacion")
    domiciliary = models.ForeignKey(Client, limit_choices_to={'role': 'domiciliary'}, related_name='domiciliary', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.client.address)


class DetailOrder(models.Model):
    order = models.ForeignKey(Order, related_name="products", on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
